package edu.uchicago.gerber._02arrays;

public class P5_24 {
    public static void main(String[] args) {

    }

    private static int romanToDeci(String roman) {
        if (roman.length() == 0) {
            return 0;
        }
        if (roman.length() == 1) {
            return helper(roman.charAt(0));
        }
        int first = helper(roman.charAt(0));
        int second = helper(roman.charAt(1));
        if (first >= second) {
            return first + romanToDeci(roman.substring(1));
        } else {
            return second - first + romanToDeci(roman.substring(2));
        }
    }

    private static int helper(char c) {
        if (c == 'I') {
            return 1;
        }
        if (c == 'V') {
            return 5;
        }
        if (c == 'X') {
            return 10;
        }
        if (c == 'L') {
            return 50;
        }
        if (c == 'C') {
            return 100;
        }
        if (c == 'D') {
            return 500;
        }
        if (c == 'M') {
            return 1000;
        }
        return 0;
    }
//    total = 0
//    str = roman number string While str is not empty
//    If str has length 1, or value(first character of str) is at least value(second character of str) Add value(first character of str) to total.
//    Remove first character from str.
//            Else
//            difference = value(second character of str) - value(first character of str) Add difference to total.
//    Remove first character and second character from str.
}
