package edu.uchicago.gerber._02arrays;
import java.util.Scanner;
import java.util.*;

public class E6_16 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        List<Integer> input = new ArrayList<>();
        // say we want 5 input values
        int count = 0;
        while (count++ < 5) {
            input.add(in.nextInt());
        }
        int max = 0;
        for (int i : input) {
            max = Math.max(max, i);
        }
        char[][] board = new char[20][input.size()];
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < input.size(); j++) {
                board[i][j] = '*';
            }
        }
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            values.add((int)(20 - 20 * (input.get(i) / (double)max)));
        }
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < input.size(); j++) {
                int needed = values.get(j);
                if (needed > i) {
                    board[i][j] = ' ';
                }
            }
        }
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println();
        }
    }
}
