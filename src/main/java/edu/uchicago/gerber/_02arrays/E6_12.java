package edu.uchicago.gerber._02arrays;

import java.util.Arrays;

public class E6_12 {
    public static void main(String[] args) {
        int[] array = new int[20];
        for (int i = 0; i < 20; i++) {
            array[i] = (int)(Math.random() * 100);
        }
        for (int i : array) {
            System.out.print(i + " ");
        }
        Arrays.sort(array);
        System.out.println();
        for (int i : array) {
            System.out.print(i + " ");
        }
    }
}
