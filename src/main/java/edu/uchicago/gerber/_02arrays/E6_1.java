package edu.uchicago.gerber._02arrays;
import java.util.Random;

public class E6_1 {
    public static void main(String[] args) {
        Random rand = new Random();
        int[] array = new int[10];
        for (int i = 0; i < 10; i++) {
            array[i] = rand.nextInt(Integer.MAX_VALUE);
        }
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            if (array[i] % 2 == 0) {
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
        for (int i = 9; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        System.out.print(array[0] + " ");
        System.out.print(array[9] + " ");
    }
}
