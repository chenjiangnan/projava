package edu.uchicago.gerber._02arrays;
import java.io.*;
import java.util.*;

public class E7_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the input file name: ");
        String input_file = input.next();
        System.out.print("Enter the output file name: ");
        String output_file = input.next();
        try {
            File inFile = new File(input_file);
            Scanner scan = new Scanner(inFile);
            PrintWriter outFile = new PrintWriter(output_file);
            int count = 1;
            while (scan.hasNextLine()) {
                String lines = scan.nextLine();
                outFile.println("/* " + count + " */ " + lines);
                count++;
            }
            outFile.close();
        } catch (IOException e) {
            System.out.println("Error processing file: " + e);
        }
    }
}
