package edu.uchicago.gerber._02arrays;
import java.util.*;
import java.io.*;

public class P7_5 {
    static List<List<String>> input = new ArrayList<>();
    public static void main(String[] args) {
        try {
            String filePath = "CSV_File_Path";
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line = null;
            while ((line = reader.readLine()) != null) {
                String item [] = line.split("，");
                input.add(Arrays.asList(item));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static int numberOfRows() {
        return input.size();
    }
    private static int numberOfFields(int row) {
        return input.get(row).size();
    }
    private static String field(int row, int column) {
        return input.get(row).get(column);
    }
}

