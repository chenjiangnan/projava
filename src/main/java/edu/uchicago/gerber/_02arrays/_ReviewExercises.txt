#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R5.6 Parameters and return values.  The difference between an argument and a return value is that an argument is passed
into a method, whereas a return value is the value returned from it.

You can have n number of arguments, whereas you can only have one return value. By varying either the number and/or type of arguments,
you change the method's signature, which means you may overload a method. Varying the return value only does not change the
method's signature and therefore does not qualify the method for overloading.

Since Java5, you can even specify a variable number of arguments aka varargs which looks something like this:
 private String[] someMethod(int nGrade, String... strNickNames) {//implementation};
Notice that the varargs parameter "String... strNickNames" must be listed last.  Calling this method would look something like this:
 String[] strReturnValues = someMethod(99, "Jim", "Jamie", "James", "Jamey", "Jimmy", "Jimbo");

R5.6 Parameters and return values
Arguments are actual values within the method body passed from methods when methods are called. A return value is the
value returned by a method when a method is called and executed. A method can have as many arguments as needed, but only
one return value.

R5.8 Pseudocode
First, we notice that only number from 2 to 9 are associated with letters.
Create a HashMap of 8 <Key, Value> sets of ['A', 'B', 'C'] -> 2, ['D', 'E', 'F'] -> 3....['W', 'X', 'Y', 'Z'] -> 9.
For each letter in the phone number, for each Key list, if the current letter is in one of the Key list, then we are able
to obtain the actual number.

R5.10 Variable scope
int variables i and b are only accessible in the main method (the ones that are in main). n's scope is in method f.
int variables b that is in method g are only accessible in method g, i and n's scope is only within the for loop.
The program will print 26.

R5.14 Pass by value versus pass by reference
The reason is that when calling the falseSwap method, the arguments are copied in stack, so that only the copied values
of x and y are swapped, not the original x and y that are in the main method.

R6.3 Considering loops
a. 25; b. 13; c. 12; d. a runtime exception happens as the index goes out of bound.; e. 11; f. 25; g. 12; h. -1

R6.10 Enhanced for-loop
a. for (int i : values) {
        total += i;
    }

b. for (int i : values) {
        total += i;
    }
    total -= values[0];

c. for (int i : values) {
        if (i == target) {
            we are not able to retrieve the index of the value i.
        }
    }

R6.23 Computing runs
int res = 1; // as the result is at least 1 item long
for idx i from 0 to array.length - 1
    j = i + 1;
    while j < array.length and array[j] == array[i]
        j++;
    i = j;
    update res if j - i + 1 is larger than res

R6.29 Multi-dimensional arrays
int[][] values = new int[ROWS][COLUMNS];
1. for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLUMNS; j++) {
            values[i][j] = 0;
        }
    }

2. for (int i = 0; i < ROWS; i++) {
           for (int j = 0; j < COLUMNS; j++) {
               if (j == 0) {
                    if (i % 2 == 0) {
                        values[i][j] = 0;
                    } else {
                        values[i][j] = 1;
                    }
               } else {
                   values[i][j] = values[i][j - 1] == 0 ? 1 : 0;
               }
           }
       }

 3. for (int i = 0; i < ROWS; i++) {
         for (int j = 0; j < COLUMNS; j++) {
             if (i == 0 || i == ROWS - 1) {
                values[i][j] = 0;
             }
         }
     }

  4. int sum = 0;
  for (int i = 0; i < ROWS; i++) {
          for (int j = 0; j < COLUMNS; j++) {
              sum += values[i][j];
          }
      }
      System.out.println("The sum of all element is " + sum);

   5. for (int i = 0; i < ROWS; i++) {
           for (int j = 0; j < COLUMNS; j++) {
               System.out.print(values[i][j] + " ");
           }
           System.out.println();
       }

R6.34 Understanding arrays
a. true; b. true; c. false; d. true; e. false; f. false

R7.1 Exceptions
a FileNotFoundException will be thrown; the file is created with the length 0.

R7.6 Throwing and catching
Catching : Catch block is used to handle the uncertain condition of try block. A try block is always followed by a catch
 block, which handles the exception that occurs in associated try block.
Throwing : Throwing an exception is as simple as using the "throw" statement. We specify the Exception object we
wish to throw. Every Exception includes a message which is a human-readable error description. It can often be related
 to problems with user input, server, backend, etc. Here is an example that shows how to throw an exception:

R7.7 Checked versus unchecked
Checked: are the exceptions that are checked at compile time. If some code within a method throws a checked exception,
then the method must either handle the exception or it must specify the exception using throws keyword. For example,
FileNotFoundException under IOException is a checked exception.
Unchecked: are the exceptions that are not checked at compile time. Such as any runtime exceptions, for example,
IndexOutOfBoundsException.

R7.8 Exceptions philosophy in Java
Because IndexOutOfBoundsException is an unchecked exception which does not need a throws key word to handle.

R7.11 What is an exception object
It can handle the error by printing out a specific message and ask the user to input the correct data.

R7.15 Scanner exceptions. Explain why these are either checked or unchecked.
The Scanner constructor can throw a FileNotFoundException(checked exception). Scanner.next can throw a
 NoSuchElementException (unchecked exception). Integer.parseInt can throw a NumberFormatException (unchecked exception).





