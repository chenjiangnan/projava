package edu.uchicago.gerber._02arrays;

public class P5_8 {
    public static void main(String[] args) {

    }
    private static boolean isLeapYear(int year) {
        if ((year % 400 == 0)) {
            return true;
        }
        if (year % 4 == 0 && year % 100 != 0) {
            return true;
        }
        return false;
    }
}
