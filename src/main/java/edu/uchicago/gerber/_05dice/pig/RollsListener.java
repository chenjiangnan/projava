package edu.uchicago.gerber._05dice.pig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RollsListener implements ActionListener {

    private Player player;
    private JButton button;

    public RollsListener(Player player, JButton button) {
        this.player = player;
        this.button = button;
    }

    public void actionPerformed(ActionEvent event) {
        int rolled = (int)(Math.random() * 6 + 1);
        if (rolled > 1)
            player.setCurrentRollScore(player.getCurrentRollScore() + rolled);
        else {
            player.setCurrentRollScore(0);
        }

        button.setVisible(false);
    }
}
