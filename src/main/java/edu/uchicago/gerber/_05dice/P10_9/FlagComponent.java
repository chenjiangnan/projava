package edu.uchicago.gerber._05dice.P10_9;
import java.awt.*;
import javax.swing.*;

public class FlagComponent extends JComponent {

    public void paintComponent(Graphics g) {
        hungarian(g);
        german(g);
    }

    public void hungarian(Graphics g) {
        g.setColor(Color.RED);
        g.fillRect(50, 50, 300, 50);
        g.setColor(Color.WHITE);
        g.fillRect(50, 100, 300, 50);
        g.setColor(Color.GREEN);
        g.fillRect(50, 150, 300, 50);
    }

    public void german(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(50, 250, 250, 50);
        g.setColor(Color.RED);
        g.fillRect(50, 300, 250, 50);
        g.setColor(Color.ORANGE);
        g.fillRect(50, 350, 250, 50);
    }
}
