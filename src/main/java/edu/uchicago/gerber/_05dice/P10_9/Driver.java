package edu.uchicago.gerber._05dice.P10_9;
import java.awt.Canvas;
import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class Driver {
    public static void main(String[] args) {
        JComponent jc = new FlagComponent();
        JFrame aFrame = new JFrame();
        aFrame.setSize(750, 750);
        aFrame.add(jc);
        aFrame.setVisible(true);
    }
}
