package edu.uchicago.gerber._05dice.P11_9;
import java.awt.*;
import javax.swing.*;


public class Driver {
    public static void main(String[] args) {
        JFrame frame = new CircleFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Circle Frame");
        frame.setVisible(true);
    }
}
