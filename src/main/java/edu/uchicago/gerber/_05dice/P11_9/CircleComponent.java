package edu.uchicago.gerber._05dice.P11_9;
import java.util.*;
import java.awt.*;
import javax.swing.*;

public class CircleComponent extends JComponent {
    private ArrayList<Point> centers;
    private ArrayList<Integer> radii;

    public CircleComponent() {
        centers = new ArrayList();
        radii = new ArrayList();
    }

    public void paintComponent(Graphics g) {
        for (int i = 0; i < centers.size(); i++) {
            Point p = centers.get(i);
            int radius = radii.get(i);
            g.drawOval(p.x - radius, p.y - radius, 2 * radius, 2 * radius);
        }
    }

    public void addCircle(int centerX, int centerY, int radius) {
        centers.add(new Point(centerX, centerY));
        radii.add(radius);
        repaint();
    }
}
