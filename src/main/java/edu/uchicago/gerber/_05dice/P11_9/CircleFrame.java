package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

public class CircleFrame extends JFrame {
    private static final int FRAME_WIDTH = 380;
    private static final int FRAME_HEIGHT = 400;
    private CircleComponent scene;
    private int centerX, centerY, radius;

    public CircleFrame() {
        centerX = centerY = -1;
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        scene = new CircleComponent();
        scene.addMouseListener(new MousePressListener());
        scene.setBorder(new EtchedBorder());
        add(scene, BorderLayout.CENTER);
    }

    class MousePressListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            int x, y;
            x = e.getX();
            y = e.getY();
            if (centerX >= 0 && centerY >= 0) {
                int r = (int) Math.sqrt((centerX - x) * (centerX - x) + (centerY - y) * (centerY - y));
                centerY = centerX = -1;
            } else {
                centerX = x;
                centerY = y;
            }
        }
    }

}
