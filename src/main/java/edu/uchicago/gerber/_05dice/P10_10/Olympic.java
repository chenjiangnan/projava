package edu.uchicago.gerber._05dice.P10_10;
import java.awt.*;
import javax.swing.*;

public class Olympic extends JComponent {
    public void paintComponent(Graphics g) {
        fillRing(g, 50, 50, 100, 100, Color.BLUE);
        fillRing(g, 60, 60, 80, 80, Color.WHITE);
        fillRing(g, 150, 50, 100, 100, Color.BLACK);
        fillRing(g, 160, 60, 80, 80, Color.WHITE);
        fillRing(g, 250, 50, 100, 100, Color.RED);
        fillRing(g, 260, 60, 80, 80, Color.WHITE);
        drawRing(g, 100, 100, 100, 100, Color.YELLOW);
        drawRing(g, 101, 101, 98, 98, Color.YELLOW);
        drawRing(g, 102, 102, 96, 96, Color.YELLOW);
        drawRing(g, 103, 103, 94, 94, Color.YELLOW);
        drawRing(g, 104, 104, 92, 92, Color.YELLOW);
        drawRing(g, 105, 105, 90, 90, Color.YELLOW);
        drawRing(g, 106, 106, 88, 88, Color.YELLOW);
        drawRing(g, 107, 107, 86, 86, Color.YELLOW);
        drawRing(g, 108, 108, 84, 84, Color.YELLOW);
        drawRing(g, 109, 109, 82, 82, Color.YELLOW);
        drawRing(g, 110, 110, 80, 80, Color.YELLOW);
        drawRing(g, 200, 100, 100, 100, Color.GREEN);
        drawRing(g, 201, 101, 98, 98, Color.GREEN);
        drawRing(g, 202, 102, 96, 96, Color.GREEN);
        drawRing(g, 203, 103, 94, 94, Color.GREEN);
        drawRing(g, 204, 104, 92, 92, Color.GREEN);
        drawRing(g, 205, 105, 90, 90, Color.GREEN);
        drawRing(g, 206, 106, 88, 88, Color.GREEN);
        drawRing(g, 207, 107, 86, 86, Color.GREEN);
        drawRing(g, 208, 108, 84, 84, Color.GREEN);
        drawRing(g, 209, 109, 82, 82, Color.GREEN);
        drawRing(g, 210, 110, 80, 80, Color.GREEN);
    }

    public void fillRing(Graphics g, int a, int b, int c, int d, Color c1) {
        g.setColor(c1);
        g.fillOval(a, b, c, d);
    }

    public void drawRing(Graphics g, int a, int b, int c, int d, Color c1) {
        g.setColor(c1);
        g.drawOval(a, b, c, d);
    }
}
