package edu.uchicago.gerber._05dice.P10_10;
import javax.swing.*;
import java.awt.*;

public class CircleViewer {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(600, 300);
        frame.setTitle("Olympic Ring");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JComponent component = new Olympic();
        frame.add(component);
        frame.setVisible(true);
    }
}
