package edu.uchicago.gerber._03objects.P8_16;
import java.util.*;

public class Mailbox {
    private List<Message> message;

    public Mailbox() {
        message = new ArrayList<>();
    }

    public void addMessage(Message m) {
        this.message.add(m);
    }

    public Message getMessage(int i) {
        return message.get(i);
    }

    public void deleteMessage(int i) {
        message.remove(i);
    }
}
