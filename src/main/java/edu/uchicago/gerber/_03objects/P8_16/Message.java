package edu.uchicago.gerber._03objects.P8_16;

public class Message {
    private String sender;
    private String recipient;
    private String text;

    public Message(String sender, String recipient) {
        this.sender = sender;
        this.recipient = recipient;
    }

    public void append(String str) {
        this.text += str;
    }

    public String toString() {
        return new String("From:" + this.sender + "%nTo" + this.recipient + "%n" + this.text);
    }
}
