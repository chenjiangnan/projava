package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {
    private int demoCount;
    private int repuCount;

    public VotingMachine() {
        this.demoCount = 0;
        this.repuCount = 0;
    }

    public void voteForDemo() {
        this.demoCount++;
    }

    public void voteForRepu() {
        this.repuCount++;
    }

    public void reset() {
        this.demoCount = 0;
        this.repuCount = 0;
    }
}
