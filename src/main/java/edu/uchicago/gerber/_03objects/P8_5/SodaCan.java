package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {
    private double radius;
    private double height;

    public SodaCan(double r, double h) {
        this.radius = r;
        this.height = h;
    }

    private double getSurfaceArea() {
        return 2 * Math.PI * this.radius * this.height + 2 * Math.PI * this.radius * this.radius;
    }

    private double getVolume() {
        return this.height * Math.PI * this.radius * this.radius;
    }
}
