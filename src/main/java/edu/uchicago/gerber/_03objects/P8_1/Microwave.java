package edu.uchicago.gerber._03objects.P8_1;

public class Microwave {
        private int time;
        private int level;

        public Microwave() {
            this.time = 0;
            this.level = 1;
        }

        private void incTimeBy30Sec() {
            this.time += 30;
        }

        private void switchLevel() {
            if (this.level == 1) {
                this.level = 2;
            } else {
                this.level = 1;
            }
        }

        private void reset() {
            this.time = 0;
            this.level = 1;
        }

        private void start() {
            System.out.println("Cooking for " + this.time + "seconds at level " + this.level);
        }
}
