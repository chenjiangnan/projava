package edu.uchicago.gerber._03objects.P8_19;
import java.util.*;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please provide an initial angle and velocity!");
        double angle = in.nextDouble();
        double velocity = in.nextDouble();
        in.close();
        Cannonball cb = new Cannonball(0);
        cb.shoot(angle, velocity);
    }
}
