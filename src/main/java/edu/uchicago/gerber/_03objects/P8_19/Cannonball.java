package edu.uchicago.gerber._03objects.P8_19;

public class Cannonball {
    private double x_pos;
    private double y_pos;
    private double x_vel;
    private double y_vel;
    private final double Gravity = -9.81;

    public Cannonball(int x) {
        this.x_pos = x;
        this.y_pos = 0;
    }

    public void move(double sec) {
        this.x_pos += sec * this.x_vel;
        this.y_pos += (this.y_vel + this.Gravity * sec) / 2 * sec;
        this.y_vel += sec * this.Gravity;
    }

    public double getX() {
        return this.x_pos;
    }

    public double getY() {
        return this.y_pos;
    }

    public void shoot(double a, double v) {
        this.x_vel = v * Math.cos(a * Math.PI / 180);
        this.y_vel = v * Math.sin(a * Math.PI / 180);
        double y = 1;
        while (y > 0) {
            move(0.1);
            System.out.println(getX() + " " + getY());
            y = this.getY();
        }
    }
}
