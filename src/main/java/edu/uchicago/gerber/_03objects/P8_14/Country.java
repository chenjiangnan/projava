package edu.uchicago.gerber._03objects.P8_14;

public class Country {
    private String name;
    private int population;
    private int area;

    public Country(String name, int population, int area) {
        this.name = name;
        this.population = population;
        this.area = area;
    }

    public int getArea() {
        return this.area;
    }

    public String getName() {
        return this.name;
    }

    public int getPopulation() {
        return this.population;
    }
}
