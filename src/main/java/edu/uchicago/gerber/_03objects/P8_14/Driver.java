package edu.uchicago.gerber._03objects.P8_14;
import java.util.*;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // for instance, read in information of 5 countries.
        System.out.println("Please give me the information for 5 countries");
        int idx = 0;
        List<Country> list = new ArrayList<>();
        while (idx++ < 5) {
            String str = in.nextLine();
            String[] s = str.split(" ");
            list.add(new Country((s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2])));
        }
        in.close();
        String countryWithLargestArea = "";
        int largestArea = 0;
        String countryWithLargestPopulation = "";
        int largestPopulation = 0;
        String countryWithLargestDensity = "";
        int largestDensity = 0;
        for (Country c : list) {
            if (c.getArea() > largestArea) {
                largestArea = c.getArea();
                countryWithLargestArea = c.getName();
            }
            if (c.getPopulation() > largestPopulation) {
                largestPopulation = c.getPopulation();
                countryWithLargestPopulation = c.getName();
            }
            if (c.getPopulation() / c.getArea() >= largestDensity) {
                largestDensity = c.getPopulation() / c.getArea();
                countryWithLargestDensity = c.getName();
            }
        }
        System.out.println("The country with the largest area is " + countryWithLargestArea);
        System.out.println("The country with the largest population is " + countryWithLargestPopulation);
        System.out.println("The country with the largest density is " + countryWithLargestDensity);
    }
}
