package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    private int _secret1;
    private int _secret2;
    private int _secret3;
    private int _status;
    private int _current;

    public ComboLock(int secret1, int secret2, int secret3) {
        _secret1 = secret1;
        _secret2 = secret2;
        _secret3 = secret3;
        _current = 0;
        _status = 0; // note: 0 = closed 1 = rightS1, 2 = 1 & leftS2, 3 = ready, 4 = open
    }

    public void reset() {
        _status = 0;
        _current = 0;
        System.out.println("Lock is re-set");
    }

    public void turnLeft(int ticks) {
        _current = _current - ticks;
        if (_current < 0) {
            _current = 39 - _current;
        }
        System.out.println("Left ticks = [" + ticks + "] and current = " + _current);
        if (_status == 1 && _current == _secret2) {
            _status = 2;

        } else _status = 0;
    }

    public void turnRight(int ticks) {
        _current = _current + ticks;
        if (_current > 39) {
            _current = _current - 39;
        }
        System.out.println("Right ticks = [" + ticks + "] and current = " + _current);
        if (_status == 0 && _current == _secret1) {
            _status = 1;
        } else if (_status == 2 && _current == _secret3) {
            _status = 3;
        } else _status = 0;
    }

    public void open() {
        if (_status == 3) {
            _status = 4;
            System.out.println("Lock is open.");
        } else {
            _status = 0;
            System.out.println("Lock does not open.");
        }
    }
}
