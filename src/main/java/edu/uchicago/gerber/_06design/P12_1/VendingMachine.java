package edu.uchicago.gerber._06design.P12_1;
import java.util.*;

public class VendingMachine {
    ArrayList<VendingMachine> productsList = new ArrayList<VendingMachine>();
    String productName;
    int availQty;
    double price;
    double machineMoney = 0;

    enum status {
        INSUFFICIENTFUNDS,
        UNAVAILABLEPRODUCT,
        OUTOFSTOCK,
        BOUGHT;
    }

    public VendingMachine() {
        this.productName = "";
        this.availQty = 0;
        this.price = 0;
    }

    public boolean addProduct(String name, int qty) {
        for (VendingMachine v : this.productsList) {
            if (v.productName.equals(name)) {
                v.availQty += qty;
                return true;
            }
        }
        return false;
    }

    public void add(String name, int qty, double price) {
        for (VendingMachine v : productsList) {
            if (v.productName.equals(name)) {
                v.availQty += qty;
                return;
            }
        }
        VendingMachine v = new VendingMachine();
        v.productName = name;
        v.availQty = qty;
        v.price = price;
        productsList.add(v);
    }

    public status buy(int idx, Coin c, int count) {
        if (idx < 0 || idx >= this.productsList.size()) {
            return status.UNAVAILABLEPRODUCT;
        }
        if (productsList.get(idx).availQty > 0 && c.getAmount(count) >= productsList.get(idx).price) {
            this.machineMoney += c.getAmount(count);
            productsList.get(idx).availQty += 1;
            return status.BOUGHT;
        } else if (productsList.get(idx).availQty <= 0) {
            return status.OUTOFSTOCK;
        } else if (c.getAmount(count) < productsList.get(idx).price) {
            return status.INSUFFICIENTFUNDS;
        }
        return status.UNAVAILABLEPRODUCT;
    }

    public void reset() {
        this.machineMoney = 0;
    }
}
