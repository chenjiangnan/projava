package edu.uchicago.gerber._06design.P12_1;

public class Coin {
    double value;

    public Coin(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }

    public double getAmount(int count) {
        return count * this.value;
    }
}
