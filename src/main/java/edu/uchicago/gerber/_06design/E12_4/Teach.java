package edu.uchicago.gerber._06design.E12_4;
import java.util.*;

public class Teach {
    boolean doneAdd;
    boolean doneSub;
    int attempts;
    int level;
    int scores;
    Random r;

    public Teach() {
        this.doneAdd = false;
        this.doneSub = false;
        this.attempts = 0;
        this.level = 1;
        this.scores = 0;
        this.r = new Random(1);
    }

    public void subtract() {
        if (this.doneSub) {
            return;
        }
        int x = 0;
        int y = 0;
        x = r.nextInt(9) + 1;
        y = r.nextInt(x);
        this.attempts = 0;
        while (this.attempts <= 2 && !doneSub) {
            System.out.println("Question : " + x + "- " + y + " = ");
            Scanner in = new Scanner(System.in);
            int curr = in.nextInt();
            this.attempts++;
            if (curr == x - y) {
                this.scores++;
                System.out.println("Correct!");
                if (this.scores == 5) {
                    System.out.println("This test is completed!");
                    this.doneSub = true;
                } else {
                    this.subtract();
                }
            } else {
                if (this.attempts == 2) {
                    System.out.println("Wrong answer! This test is over, not passed!");
                    this.add();
                    break;
                }
                System.out.println("Wrong answer, try it again!");
            }
        }
    }

    public void add() {
        if (this.doneAdd) {
            return;
        }
        int x = 0;
        int y = 0;
        if (this.level == 1) {
            do {
                x = r.nextInt(10);
                y = r.nextInt(10);
            } while (x + y >= 10);
        } else {
            x = r.nextInt(10);
            y = r.nextInt(10);
        }
        System.out.println("Question : " + x + "+ " + y + " = ");
        Scanner in = new Scanner(System.in);
        int curr = in.nextInt();
        while (this.attempts <= 2 && !doneAdd) {
            this.attempts++;
            if (curr == x + y) {
                this.scores++;
                System.out.println("Correct!");
                if (this.scores == 5) {
                    System.out.println("You have moved to the next Level!");
                    this.scores = 0;
                    this.level++;
                    if (this.level < 3) {
                        this.add();
                    } else {
                        doneAdd = true;
                    }
                } else {
                    this.add();
                }
            } else {
                if (this.attempts == 2) {
                    System.out.println("Wrong answer! This test is over, not passed!");
                    this.add();
                    break;
                } else {
                    System.out.println("Wrong answer! Try again!");
                }
            }
        }
    }
}
