package edu.uchicago.gerber._07streams;
import java.util.*;

public class E13_20 {
    public static void main(String[] args) {
        System.out.println(waysOfPaying(40));
    }
    public static List<List<Integer>> waysOfPaying(int price) {
        List<List<Integer>> res = new ArrayList<>();
        helper(res, new ArrayList<>(), 0, price);
        return res;
    }
    private static void helper(List<List<Integer>> res, List<Integer> list, int curr, int price) {
        if (curr > price) {
            return;
        }
        if (curr == price) {
            Collections.sort(list);
            if (!res.contains(list)) {
                res.add(new ArrayList<>(list));
            }
            return;
        }
        list.add(100);
        helper(res, list, curr + 100, price);
        list.remove(list.size() - 1);
        list.add(20);
        helper(res, list, curr + 20, price);
        list.remove(list.size() - 1);
        list.add(5);
        helper(res, list, curr + 5, price);
        list.remove(list.size() - 1);
    }
}
