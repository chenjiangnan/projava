package edu.uchicago.gerber._07streams;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class E19_5 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        Stream<Integer> stream = list.stream();
        System.out.println(toString(stream, 3));
    }
    private static String toString(Stream<Integer> stream, int n) {
        Stream<String> stringStream = stream.limit(n).map(str -> str.toString());
        return stringStream.collect(Collectors.joining(", "));
    }
}
