package edu.uchicago.gerber._07streams;
import java.util.*;

public class YodaSpeak {
    public static void main(String[] args) {
        System.out.println("Please type a sentence!");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        System.out.println(reverseWords(s));
    }
        public static String reverseWords(String s) {
            s = s.trim();
            StringBuilder sb = new StringBuilder();
            char[] c = s.toCharArray();
            for (int i = 0; i < c.length; i++) {
                if (c[i] != ' ') {
                    sb.append(c[i]);
                } else if (c[i] != c[i - 1]) {
                    sb.append(c[i]);
                }
            }
            sb.reverse(); // apple pear tea -> eat reap elppa
            int i = 0;
            while (i < sb.length()) {
                int j = i;
                while (j < sb.length() && sb.charAt(j) != ' ') {
                    j++;
                }
                if (j == sb.length()) {
                    reverse(sb, i, j - 1);
                    break;
                }
                reverse(sb, i, j - 1);
                i = j + 1;
            }
            return sb.toString();
        }
        private static void reverse(StringBuilder sb, int i, int j) {
            while (i < j) {
                char temp = sb.charAt(i);
                sb.setCharAt(i, sb.charAt(j));
                sb.setCharAt(j, temp);
                i++;
                j--;
            }
        }
}
