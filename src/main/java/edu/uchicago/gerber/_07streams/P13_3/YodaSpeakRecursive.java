package edu.uchicago.gerber._07streams.P13_3;

import java.util.Scanner;

public class YodaSpeakRecursive {
    public static void main(String[] args) {
        System.out.println("Please type a sentence!");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String str = reverseWords(s);
        System.out.println(helper(s));
    }
    private static String reverseWords(String s) {
        if (s.length() == 1) {
            return s;
        }
        return reverseWords(s.substring(1, s.length())) + s.charAt(0);
    }
    private static String helper(String s) {
        s = reverseWords(s);
        String[] strs = s.split(" ");
        String res = "";
        for (String st : strs) {
            res += reverseWords(st) + " ";
        }
        res.trim();
        return res;
    }
}
