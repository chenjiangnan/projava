package edu.uchicago.gerber._07streams.P13_3;
import java.util.*;

public class PhoneNumberSpelling {
    public List<String> ways(String num) {
        // since '0' and '1' do not refer to any characters, we will use '$' to refer to 0 and '#' to refer to 1
        String[][] map = {{"$"}, {"#"}, {"A", "B", "C"}, {"D", "E", "F"}, {"G", "H", "I"}, {"J", "K", "L"},
                {"M", "N", "O"}, {"P", "Q", "R", "S"}, {"T", "U", "V"}, {"W", "X", "Y", "Z"}};
        List<String> res = new ArrayList<>();
        helper(map, res, num, 0, new StringBuilder());
        return res;
    }
    private void helper(String[][] map, List<String> res, String num, int idx, StringBuilder sb) {
        if (idx == num.length()) {
            res.add(sb.toString());
        } else {
            for (String s : map[num.charAt(idx) - '0']) {
                sb.append(s);
                helper(map, res, num, idx + 1, sb);
                sb.deleteCharAt(sb.length() - 1);
            }
        }
    }
}
