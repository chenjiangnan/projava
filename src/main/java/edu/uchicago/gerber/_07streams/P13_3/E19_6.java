package edu.uchicago.gerber._07streams.P13_3;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_6 {
    public static void main(String[] args) {
        Set<Currency> currency = Currency.getAvailableCurrencies();
        Stream<Currency> currencyStream = currency.stream();
        Stream<String> currencyNames = currencyStream.map(current -> current.getDisplayName());
        List<String> sortedCurrency = currencyNames.sorted().collect(Collectors.toList());
        for (String current: sortedCurrency) {
            System.out.println(current);
        }
    }
}
