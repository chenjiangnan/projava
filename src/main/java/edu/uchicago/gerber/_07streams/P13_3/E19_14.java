package edu.uchicago.gerber._07streams.P13_3;
import java.util.*;
import java.util.stream.Stream;
import java.io.*;

public class E19_14 {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        try (Scanner fileInput = new Scanner(new File("words.txt"))) { // here , we assume the input file is called "words.txt"
            while (fileInput.hasNext()) {
                words.add(fileInput.next());
            }
            String PalindromicWord = words.stream().parallel().filter(str -> str.length() >= 5).filter(str ->
                    isPalindrome(str)).findAny().orElse("No palindromes at least 5 in length found!");
            System.out.println("The valid word is : " + PalindromicWord);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    private static boolean isPalindrome(String str) {
        StringBuilder sb = new StringBuilder();
        sb = sb.reverse();
        return str.equals(sb.toString());
    }
}
