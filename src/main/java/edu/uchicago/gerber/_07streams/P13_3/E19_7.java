package edu.uchicago.gerber._07streams.P13_3;
import java.io.IOException;
import java.io.FileNotFoundException ;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;


//Main class
public class E19_7
{
    public static void main(String[] args) throws IOException,FileNotFoundException {
        String filename="file.txt"; // assuming there is an input file called "file.txt"
        try {
            //get the data and store in lines, use map and flatMap to split into words
            Stream <String> lines = Files.lines(Paths.get(filename)).
                    map(line -> line.split("[\\s]+")).
                    flatMap(Arrays::stream).distinct();
            //convert each word to required format and print using forEach method
            lines.forEach(y->System.out.print(y.charAt(0)+"..."+y.charAt(y.length()-1)+" "));
            lines.close();
        } catch (FileNotFoundException e) {
            System.out.print("File "+filename+" not found");
        }
    }

}