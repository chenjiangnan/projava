package edu.uchicago.gerber._07streams.P13_3;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_16 {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        try (Scanner fileInput = new Scanner(new File("input1.txt"));) {
            while (fileInput.hasNext()) {
                words.add(fileInput.next());
            }
            System.out.println("All words are: ");
            for (String s : words) {
                System.out.println(s);
            }
            System.out.println();
            Stream<String> wordStream = words.stream();
            Map<String, Double> groups = wordStream.collect(Collectors.groupingBy(str -> str.toLowerCase().substring(0, 1), Collectors.averagingDouble(String::length
            )));
            System.out.println("Result key value pairs are : ");
            for (Map.Entry<String, Double> current: groups.entrySet()) {
                System.out.println(current.getKey() + ":" + current.getValue());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
