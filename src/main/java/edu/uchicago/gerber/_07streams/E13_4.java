package edu.uchicago.gerber._07streams;

public class E13_4 {
    public static void main(String[] args) {
        System.out.println(decToBinary(2));
        System.out.println(decToBinary(5));
        System.out.println(decToBinary(18));
        System.out.println(decToBinary(128));

    }
    private static String decToBinary(int number) {
        if (number == 0) {
            return "";
        }
        if (number % 2 == 0) {
            return decToBinary(number / 2) + "0";
        } else {
            return decToBinary(number / 2) + "1";
        }
    }
}
