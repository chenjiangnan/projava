package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class P3_14 {
    public static void main(String[] args) {
        System.out.println("Enter a year");
        Scanner in  = new Scanner(System.in);
        int year = in.nextInt();
        if ((year % 400 == 0) || (year % 4 == 0 && (year % 100 != 0))) {
            System.out.println("Yes, it is a leap year");
        } else {
            System.out.println("No, it is not a leap year");
        }
    }
}
