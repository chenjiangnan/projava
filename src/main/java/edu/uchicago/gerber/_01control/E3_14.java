package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class E3_14 {
    public static void main(String[] args) {
        System.out.println("Enter the month");
        System.out.println("Enter the day");
        Scanner in = new Scanner(System.in);
        int month = in.nextInt();
        int day = in.nextInt();
        if ((month == 1) || (month == 2) || (month == 3)) {
            if (month % 3 == 0 && day >= 21) {
                System.out.println("The season is : Spring");
            } else {
                System.out.println("The season is : Winter");
            }
        } else if ((month == 4) || (month == 5) || (month == 6)) {
            if (month % 3 == 0 && day >= 21) {
                System.out.println("The season is : Summer");
            } else {
                System.out.println("The season is : Spring");
            }
        } else if ((month == 7) || (month == 8) || (month == 9)) {
            if (month % 3 == 0 && day >= 21) {
                System.out.println("The season is : Fall");
            } else {
                System.out.println("The season is : Summer");
            }
        } else if ((month == 10) || (month == 11) || (month == 12)) {
            if (month % 3 == 0 && day >= 21) {
                System.out.println("The season is : Winter");
            } else {
                System.out.println("The season is : Fall");
            }
        }
    }
}
