package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class E2_4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the first integer!");
        int first = in.nextInt();
        System.out.println("Please enter the second integer!");
        int second = in.nextInt();
        System.out.println("The sum of these 2 integers is : " + (first + second));
        System.out.println("The difference of these 2 integers is : " + (first - second));
        System.out.println("The product of these 2 integers is : " + first * second);
        System.out.println("The average of these 2 integers is : " + (first + second) / 2.0);
        System.out.println("The distance of these 2 integers is : " + Math.abs(first - second));
        System.out.println("The maximum of these 2 integers is : " + Math.max(first, second));
        System.out.println("The minimum of these 2 integers is : " + Math.min(first, second));
    }
}
