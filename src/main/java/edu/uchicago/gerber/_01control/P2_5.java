package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args) {
        System.out.println("Enter the amount");
        Scanner in = new Scanner(System.in);
        double money = in.nextDouble();
        int money1 = (int)(money * 100 + 0.5);
        System.out.println("It is " + (money1 / 100) + " dollars" + " and " + (money1 % 100) + " cents");
    }
}
