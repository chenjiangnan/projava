package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class P3_7 {
    public static void main(String[] args) {
//        • 1 percent on the first $50,000.
//• 2 percent on the amount over $50,000 up to $75,000.
//• 3 percent on the amount over $75,000 up to $100,000.
//• 4 percent on the amount over $100,000 up to $250,000.
//• 5 percent on the amount over $250,000 up to $500,000.
//• 6 percent on the amount over $500,000.
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your income in USD:");
        double income = in.nextDouble();
        double tax;
        if (income <= 50000) {
            tax = income * 0.01;
        } else if (income <= 75000) {
            tax = 500 + (income - 50000) * 0.02;
        } else if (income <= 100000) {
            tax = 1000 + (income - 75000) * 0.03;
        } else if (income <= 250000) {
            tax = 1750 + (income - 100000) * 0.04;
        } else if (income <= 500000) {
            tax = 7750 + (income - 250000) * 0.05;
        } else {
            tax = 20250 + (income - 500000) * 0.06;
        }
        System.out.println("Your income tax is " + tax);
    }
}
