package edu.uchicago.gerber._01control;

public class E2_20 {
    public static void main(String[] args) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                if (i + j == 3) {
                    System.out.print("/");
                } else if (j == i + 4) {
                    System.out.print("\\");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        for (int i = 0; i < 8; i++) {
            System.out.print("-");
        }
        System.out.println();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 8; j++) {
                if (j == 2 || j == 5) {
                    System.out.print("\"");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
