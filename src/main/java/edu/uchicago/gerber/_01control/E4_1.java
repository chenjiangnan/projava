package edu.uchicago.gerber._01control;
import java.util.*;

public class E4_1 {
    public static void main(String[] args) {
        int sumA = 0;
        for (int i = 2; i <= 100; i += 2) {
            sumA += i;
        }
        System.out.println("The answer to a is + " + sumA);
        int sumB = 0;
        for (int i = 1; i * i <= 100; i++) {
            sumB += i * i;
        }
        System.out.println("The answer to b is + " + sumB);
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i <= 20; i++) {
            res.add((int)Math.pow(2, i));
        }
        System.out.println("The answer to c is + " + res);
        System.out.println("Enter two numbers a and b");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int sumD = 0;
        for (int i = a; i <= b; i++) {
            sumD += i % 2 == 0 ? 0 : i;
        }
        System.out.println("The answer to d is " + sumD);
        System.out.println("Enter a number");
        int sumE = 0;
        int input = in.nextInt();
        while (input != 0) {
            int temp = input % 10;
            sumE += (temp % 2 == 0 ? 0 : temp);
            input /= 10;
        }
        System.out.println("The answer to e is " + sumE);
    }
}
