package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class P3_13 {
    public static void main(String[] args) {
        System.out.println("Enter an integer");
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] roman = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            int base = num / values[i];
            while (base-- != 0) {
                sb.append(roman[i]);
            }
            num = num % values[i];
        }
        System.out.println("The converted Roman number is " + sb.toString());
    }
}
