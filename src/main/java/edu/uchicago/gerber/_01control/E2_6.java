package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class E2_6 {
    public static void main(String[] args) {
        double meters;
        final double MeterToMile = 0.000621371192;
        final double MeterToFeet = 3.2808399;
        final double MeterToInch = 39.3700787;
        System.out.println("Please enter the meters");
        Scanner in = new Scanner(System.in);
        meters = in.nextDouble();
        System.out.println("It is " + meters * MeterToMile + "miles");
        System.out.println("It is " + meters * MeterToFeet + "feet");
        System.out.println("It is " + meters * MeterToInch + "inches");
    }
}
