package edu.uchicago.gerber._01control;
import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number");
        int input = in.nextInt();
        StringBuilder sb = new StringBuilder();
        while (input != 0) {
            sb.append(input % 2 == 0 ? 0 : 1);
            input /= 2;
        }
        System.out.println(sb.reverse());
    }
}
