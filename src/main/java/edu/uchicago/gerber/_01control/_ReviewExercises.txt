#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R1.5 The following are all benefits to using Java over machine code:
1/ Java bytecode is platform independent and may be run on any system that has a JVM. This quality is known as portability.
2/ Java JIT compilers can run efficient Java programs as fast or faster than machine code programs
3/ Java is memory-managed. This reduces about 95% of runtime errors (mostly memory-related) as compared to unmanaged code.
4/ Java was designed for networking and the Internet in mind; and it is generally safer than machine code
5/ With the introduction of Java8, Java is now a declarative and functional programming language.

Review Exercises: 3 points each. Place your answers in ReviewExercises.txt located in the root of this project.
R1.5 Benefits of using Java over machine code (This one is already answered for you)

R2.4 Translating math expressions to Java.
1. s = s0 + vo * t + 0.5 * g * Math.pow(t, 2)
2. FV = PV * Math.pow(1 + Integer.MAX_VALUE / 100, YRS)
3. G = 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) / Math.pow(p, 2) / (m1 + m2)
4. c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2) - 2 * a * b * Math.cos(gamma))


R2.7 Understanding integer operations, order of precedence, and integer division.
a. 8
b. 1
c. 17
d. 17.5
e. 17
f. 18

R2.14 How the compiler interprets primitive values
2 is an Integer, 2.0 is a Double, '2' is a Character, "2" is a String
"2.0" is a String

R2.17 Pseudocode understading
First, create a Scanner Object to read the input string, then store the input string into a String object. This
String object we just created contains the full name of that given person. Then, we split the string into an array of
3 strings, storing the first, middle, and last name of that given person. Eventually, for that three strings, we just need
to get the first character, which is done by String.charAt(0) method. Then we add these 3 characters and combine them into
a single string. Done.

R2.22 More pseudocode - you do not need to draw a diagram for this question
when the day number is 4, the position will be 12, that is the position of the first character of "Thursday". Then we just
need to do a String.substring method to get that substring. substring(12, 15) which is "Thu"

R3.19 Pseudocode for grade problem
If the score is larger than or equal to 90 and smaller than or equal to 100, output A.
Else if the score is larger than or equal to 80 and smaller than or equal to 89, output B.
Else if the score is larger than or equal to 70 and smaller than or equal to 79, output C.
Else if the score is larger than or equal to 60 and smaller than or equal to 69, output D.
Else if the score is less than 60, output F.

R3.30 Boolean expressions
a. false
b. true
c. true
d. true
e. false
f. false
g. false
h. true

R4.12 Implementing loops
There are three different loops in Java language, which are For loop, While loop, and Do while loop.
We use for loop when we know exactly how many times we will need to do an action. We use a while loop when
we want to keep doing something until our counter reach a certain limit so that we will stop afterwards. We
use the do while loop when we want to at least carry out one action before incrementing or decrementing our
counter.

R4.14 Calendar problem
First, print out a line "Su M T W Th F Sa". Then start a newline. Use a for loop with i from 1 to 35(exclusive). If
i is 1 or 2 or 3, print "    "; Else, print day(starts from 1), day++; if day is divisible by 7, start a newline.
