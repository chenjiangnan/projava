package edu.uchicago.gerber._04interfaces.P9_6;

public class Monthly extends Appointment{
    public Monthly(String d, String date) {
        super(d, date);
    }
    public boolean occursOn(int year, int month, int day) {
        String[] strs = this.date.split("\\|"); // 2021|12|25 format
        if (strs[1].equals("" + month) && strs[2].equals("" + day)) {
            return true;
        }
        return false;
    }
}
