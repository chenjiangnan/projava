package edu.uchicago.gerber._04interfaces.P9_6;

public class Appointment {
    String description;
    String date;

    public Appointment(String d, String date) {
        this.description = d;
        this.date = date;
    }

    public boolean occursOn(int year, int month, int day) {
        String[] strs = this.date.split("\\|"); // 2021|12|25 format
        if (strs[0].equals("" + year) && strs[1].equals("" + month) && strs[2].equals("" + day)) {
            return true;
        }
        return false;
    }
}
