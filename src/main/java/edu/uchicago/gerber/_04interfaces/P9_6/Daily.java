package edu.uchicago.gerber._04interfaces.P9_6;

public class Daily extends Appointment{
    public Daily(String d, String date) {
        super(d, date);
    }
    public boolean occursOn(int year, int month, int day) {
        return true;
    }
}
