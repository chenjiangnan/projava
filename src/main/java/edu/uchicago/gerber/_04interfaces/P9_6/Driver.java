package edu.uchicago.gerber._04interfaces.P9_6;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;


public class Driver {
    public static void main(String[] args) {
        Appointment a = new Daily("Meeting1", "2021|1|12");
        Appointment b = new Onetime("Meeting2", "2021|1|12");
        Appointment c = new Monthly("Meeting3", "2021|1|12");
        Appointment d = new Monthly("Meeting4", "2021|2|12");
        Appointment e = new Monthly("Meeting5", "2021|4|12");
        Appointment f = new Monthly("Meeting6", "2021|6|12");
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a date: Format is Year|Month|Day");
        String date = in.next();
        String[] strs = date.split("\\|");
        int year = Integer.parseInt(strs[0]);
        int month = Integer.parseInt(strs[1]);
        int day = Integer.parseInt(strs[2]);
        List<Appointment> list = new ArrayList<>();
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);
        list.add(f);
        System.out.println("All appointments that occur on this date are : ");
        for (Appointment x : list) {
            if (x.occursOn(year, month, day)) {
                System.out.println(x.description);
            }
        }
    }

}
