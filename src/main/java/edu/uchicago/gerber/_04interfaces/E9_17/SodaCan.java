package edu.uchicago.gerber._04interfaces.E9_17;

public class SodaCan implements Measurable {
    private double radius;
    private double height;

    public SodaCan(double r, double h) {
        this.radius = r;
        this.height = h;
    }

    private double getSurfaceArea() {
        return 2 * Math.PI * this.radius * this.height + 2 * Math.PI * this.radius * this.radius;
    }

    private double getVolume() {
        return this.height * Math.PI * this.radius * this.radius;
    }

    @Override
    public double surfaceArea() {
        return 2 * Math.PI * this.radius * this.height + 2 * Math.PI * this.radius * this.radius;
    }
}
