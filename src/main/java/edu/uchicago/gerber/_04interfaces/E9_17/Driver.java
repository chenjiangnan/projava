package edu.uchicago.gerber._04interfaces.E9_17;

public class Driver {
    public static void main(String[] args) {
        SodaCan a = new SodaCan(1, 2);
        SodaCan a1 = new SodaCan(2, 2);
        SodaCan a2 = new SodaCan(2, 3);
        SodaCan a3 = new SodaCan(1, 4);
        SodaCan a4 = new SodaCan(5, 3);
        SodaCan a5 = new SodaCan(3, 2);
        SodaCan[] input = new SodaCan[6];
        input[0] = a;
        input[1] = a1;
        input[2] = a2;
        input[3] = a3;
        input[4] = a4;
        input[5] = a5;
        double totalSurfaceArea = 0;
        for (SodaCan s : input) {
            totalSurfaceArea += s.surfaceArea();
        }
        System.out.println("The average surface area is " + totalSurfaceArea / 6);
    }
}
