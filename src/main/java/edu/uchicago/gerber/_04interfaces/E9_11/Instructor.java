package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person{
    private int salary;

    public Instructor(String name, int yearOfBirth, int salary) {
        super(name, yearOfBirth);
        this.salary = salary;
    }

    public void setSalary(int s) {
        this.salary = s;
    }

    public int getSalary() {
        return this.salary;
    }
}
