package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {
    private String name;
    private int yearOfBirth;

    public Person(String name, int yearOfBirth) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public String toString() {
        return "" + this.name + this.yearOfBirth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setYearOfBirth(int y) {
        this.yearOfBirth = y;
    }

    public int getYearOfBirth() {
        return this.yearOfBirth;
    }
}
