package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person {
    private String major;

    public Student(String name, int yearOfBirth, String major) {
        super(name, yearOfBirth);
        this.major = major;
    }

    public String toString() {
        return this.getName() + this.major;
    }

    public void setMajor(String m) {
        this.major = m;
    }

    public String getMajor() {
        return this.major;
    }
}
