package edu.uchicago.gerber._04interfaces.E9_11;

public class Driver {
    public static void main(String[] args) {
        Person nick = new Instructor("Nick", 1992, 100000);
        Person mark = new Student("Mark", 1993, "Physics");
        System.out.println(nick.toString());
        System.out.println(mark.toString());
    }
}
