package edu.uchicago.gerber._04interfaces.E9_8;

public class BankAccount {
    private double currentBalance;

    public BankAccount(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public void deposit(double amount) {
        this.currentBalance += amount;
    }

    public void withdraw(double amount) {
        this.currentBalance -= amount;
    }

    public void monthEnd() {
        System.out.println(this.currentBalance);
    }

    public double getBalance() {
        return this.currentBalance;
    }
}
