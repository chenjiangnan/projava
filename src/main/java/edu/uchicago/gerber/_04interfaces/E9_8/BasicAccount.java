package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount {

    public BasicAccount(double currentBalance) {
        super(currentBalance);
    }

    @Override
    public void withdraw(double amount) {
        if (amount <= super.getBalance()) {
            super.withdraw(amount);
        }
    }
}
