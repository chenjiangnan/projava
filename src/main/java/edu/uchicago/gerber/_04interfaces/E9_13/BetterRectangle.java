package edu.uchicago.gerber._04interfaces.E9_13;

import java.awt.*;

public class BetterRectangle extends Rectangle {

    public BetterRectangle(int x, int y) {
        super.setLocation(x, y);
        super.getSize();
    }

    public double getPerimeter() {
        return 2 * (this.x + this.y);
    }

    public double getArea() {
        return this.x * this.y;
    }
}
