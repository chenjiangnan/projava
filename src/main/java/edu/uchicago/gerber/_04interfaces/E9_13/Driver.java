package edu.uchicago.gerber._04interfaces.E9_13;

public class Driver {
    public static void main(String[] args) {
        BetterRectangle b = new BetterRectangle(100, 100);
        System.out.println(b.getArea());
        System.out.println(b.getPerimeter());
    }
}
