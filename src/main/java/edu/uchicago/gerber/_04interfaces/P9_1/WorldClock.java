package edu.uchicago.gerber._04interfaces.P9_1;

public class WorldClock extends Clock {
    private int offset;
    public WorldClock(int offset) {
        this.offset = offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return this.offset;
    }
    // the getHours method at subclass WorldClock is overridden
    public String getHours() {
        int a = Integer.parseInt(super.getHours());
        a = (a + this.offset) % 24;
        return "" + a;
    }
}
