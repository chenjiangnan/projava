package edu.uchicago.gerber._04interfaces.P9_1;

import java.time.LocalTime;

public class Clock {

    public String getHours() {
         String hour = java.time.LocalTime.now().toString();
         String[] time = hour.split(":");
         return time[0];
    }
    public String getMinutes() {
        String hour = java.time.LocalTime.now().toString();
        String[] time = hour.split(":");
        return time[1];
    }

    public String getTime() {
        return this.getHours() + ":" + this.getMinutes();
    }

}
